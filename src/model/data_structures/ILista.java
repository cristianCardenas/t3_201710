package model.data_structures;

public interface ILista<T> extends Iterable<T>{
	
	/**
	 * A�ade el elemento elem al final de la lista
	 * @param elem
	 */
	public void agregarElementoFinal(T elem);
	
	/**
	 * Retorna el elemento en la posici�n pos
	 * @param pos posici�n a buscar en la lista
	 * @return elemento en la posici�n pos
	 * @throws NoExisteElemento 
	 */
	public T darElemento(int pos) throws NoExisteElemento;
	
	/**
	 * Devuelve el tama�o de la lista
	 * @return n�mero de elementos en la lista
	 */
	public int darNumeroElementos();
	
	/**
	 * Devuelve el elemento en la posici�n actual de referencia de la lista
	 * @return elemento - es el elemento guardado en la posci�n actual de referencia de la lista
	 */
	public T darElementoPosicionActual();
	
	/**
	 * Avanza la posici�n actual de referencia en una posici�n
	 * @return true si pudo avanzar, false si es la �ltima posici�n de la lista
	 */
	public boolean avanzarSiguientePosicion();
	
	/**
	 * Retrocede la posici�n actual de referencia en una posici�n 
	 * @return true si pudo retroceder, false de lo contrario.
	 */
	public boolean retrocederPosicionAnterior();

	public void quitarElemento();

	
	
	
	

}
