package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {
	private NodoDoble<T> cabeza=null;
	private NodoDoble<T>ultimo=null;
	private NodoDoble<T> nodoActual= null;
	private int size=0;
	
	@Override
	public Iterator<T> iterator() {

		if (isEmpty()) {
			return null;
		}
		return new Iterator<T>() {
			private NodoDoble<T> currentNode = null;

			@Override
			public boolean hasNext() {
				boolean hasnext=false;
				if(currentNode.darSiguiente()==null)
				{
					hasnext=false;
				}
				else if(currentNode.darSiguiente()!=null){
					hasnext=true;
				}
				else if(cabeza==null){
					hasnext=false;
				}
				return hasnext;
				
			}

			@Override
			public T next() {
				if (currentNode == null) {
					currentNode = (NodoDoble<T>) cabeza;
					currentNode= (NodoDoble<T>) ultimo;
					return currentNode.data;
				}
				if (currentNode.darSiguiente() == null) {
					try {
						throw new NoExisteElemento("Siguiente de :"+currentNode.toString());
					} catch (NoExisteElemento e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
				currentNode = currentNode.darSiguiente();
			}
				return currentNode.data;
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		NodoDoble<T> nodoActual= (NodoDoble<T>) cabeza;
		if(nodoActual.darSiguiente()==null)
		{
			nodoActual.setSiguiente((NodoDoble<T>) elem);
			nodoActual.darSiguiente().setAnterior(nodoActual);
		}
		else
		{
			nodoActual= nodoActual.darSiguiente();
			agregarElementoFinal(elem);
		}
size++;
		// TODO Auto-generated method stub

	}

	@Override
	public T darElemento(int pos) throws NoExisteElemento {
		NodoDoble<T> nodo= (NodoDoble<T>) cabeza;
		int contador=0;
		while(contador!=pos&& nodo.darSiguiente()!=null)
		{
			nodo= nodo.darSiguiente();
			contador++;
			if(pos>contador&&nodo.darSiguiente()==null)
			{

				throw new NoExisteElemento("No existe el nodo en la posicion:"+pos);
			}
			break;
		}

		return (T) nodo;


	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {

		return (T) nodoActual;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean  avanza=false;
		if(nodoActual.darSiguiente()!=null)
		{
			avanza =true;
			nodoActual= nodoActual.darSiguiente();
		}
		return avanza;
	}
	public boolean isEmpty()
	{ 
		return size == 0; 
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		boolean  avanza=false;
		if(nodoActual.darAnterior()!=null)
		{
			avanza =true;
			nodoActual= nodoActual.darAnterior();
		}
		return avanza;
	}

	public NodoDoble<T> darPrimerElemento()
	{
		return  cabeza;
	}
	public NodoDoble<T> darUltimoElemento()
	{
		return ultimo;
	}

	@Override
	public void quitarElemento() {
		// TODO Auto-generated method stub
		
	}

}