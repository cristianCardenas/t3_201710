package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> 
{
	private NodoSencillo<T> cabeza;
	private int nodoActual = 0;
	private int size = 0;

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>()
				{
			NodoSencillo<T> actual = null;

			public boolean hasNext() 
			{
				if(size == 0)
					return false;

				if(actual == null)
					return true;

				return actual.darSiguiente() != null;
			}

			@Override
			public T next()
			{
				if(actual == null)
					actual = cabeza;
				else
					actual = actual.darSiguiente();

				return actual.getItem();
			}

			@Override
			public void remove() 
			{
				// TODO Auto-generated method stub

			}
				};
	}

	public void agregarElementoFinal(T elem) 
	{
		NodoSencillo<T> nuevoNodo = new NodoSencillo<T>(elem);
		if (cabeza == null)
		{
			cabeza = nuevoNodo;
		}
		else
		{
			NodoSencillo<T> actual = cabeza;
			while (actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			actual.setSiguiente(nuevoNodo);
		}
		size++;
	}

	@Override
	public T darElemento(int pos) throws ArrayIndexOutOfBoundsException
	{
		T elem = null;
		if(pos >= size)
		{
			throw new ArrayIndexOutOfBoundsException("No existe la posici�n ingresada");
		}
		else
		{
			int cont = 0;
			NodoSencillo<T> actual = cabeza;
			while(actual != null)
			{
				if(cont == pos)
				{
					elem = actual.getItem();
				}
				actual = actual.darSiguiente();
				cont++;
			}
		}
		return elem;
	}

	@Override
	public int darNumeroElementos() 
	{
		return size;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		return darElemento(nodoActual);
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		boolean resultado = false;
		if(nodoActual + 1 >= size)
			resultado = false;
		else
		{
			nodoActual++;
			resultado = true;
		}

		return resultado;
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		boolean resultado = false;
		if(nodoActual - 1 >= 0 && nodoActual - 1 <= size)
		{
			resultado = true;
			nodoActual--;
		}
		return resultado;
	}
	public void quitarElemento()
	{
		
	}

}
