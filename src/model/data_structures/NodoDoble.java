package model.data_structures;

public class NodoDoble <T> {
	
	private NodoDoble <T> anterior;
	private NodoDoble<T> siguiente;
	public T data=null;
	public NodoDoble<T> next;
	public NodoDoble()
	{
		siguiente=null;
		anterior=null;
	}

	 private void Node(T data){
	        this.data = data;
	    }
	public NodoDoble<T> darSiguiente()
	{
		return siguiente;
		
	}
	
	public NodoDoble<T> darAnterior()
	{
		return anterior;
		
	}

	public void setSiguiente(NodoDoble<T> siguiente)
	{
	this.siguiente= siguiente;
	}
	public void setAnterior (NodoDoble<T> anterior)
	{
		this.anterior=anterior;
	}
}