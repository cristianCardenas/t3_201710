package model.data_structures;

import java.util.Iterator;

public class Stack<Item, T> implements Iterable<Item> {
    private NodoDoble<T> first;     // top of stack
    private int n;                // size of the stack

    // helper linked list class
    private static class Node<Item> {
        private Item item;
        private Node<Item> next;
    }

    /**
     * Initializes an empty stack.
     */
    public Stack() {
        first = null;
        n = 0;
    }
   
		// TODO Auto-generated method stub

    /**
     * Returns true if this stack is empty.
     *
     * @return true if this stack is empty; false otherwise
     */
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * Returns the number of items in this stack.
     *
     * @return the number of items in this stack
     */
    public int size() {
        return n;
    }

    /**
     * Adds the item to this stack.
     *
     * @param  item the item to add
     */
    public void push(Item item) {
       NodoDoble<T> oldfirst = first;
        first = new NodoDoble<>();
        first.data = (T) item;
        first.setSiguiente(oldfirst); 
        n++;
    }

    /**
     * Removes and returns the item most recently added to this stack.
     *
     * @return the item most recently added
     * @throws NoExisteElemento 
     * @throws NoSuchElementException if this stack is empty
     */
    public Item pop() throws NoExisteElemento {
        if (isEmpty()) throw new NoExisteElemento("No");
        Item item = (Item) first.data;        // save item to return
        first = first.next;            // delete first node
        n--;
        return item;                   // return the saved item
    }


    /**
     * Returns (but does not remove) the item most recently added to this stack.
     *
     * @return the item most recently added to this stack
     * @throws NoExisteElemento 
     * @throws NoSuchElementException if this stack is empty
     */
    public Item peek() throws NoExisteElemento {
        if (isEmpty()) throw new NoExisteElemento("No");
        return (Item) first.data;
    }

    /**
     * Returns a string representation of this stack.
     *
     * @return the sequence of items in this stack in LIFO order, separated by spaces
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Item item : this) {
            s.append(item);
            s.append(' ');
        }
        return s.toString();
    }
       

    /**
     * Returns an iterator to this stack that iterates through the items in LIFO order.
     *
     * @return an iterator to this stack that iterates through the items in LIFO order
     */
    public Iterator<Item> iterator() {
        return new ListIterator<Item>(first);
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator<Item> implements Iterator<Item> {
        private NodoDoble<T> current;

        public ListIterator(NodoDoble<T> first) {
            current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext())
				try {
					throw new NoExisteElemento("No");
				} catch (NoExisteElemento e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            Item item = (Item) current.data;
            current = current.next; 
            return item;
        }
    }
}


  