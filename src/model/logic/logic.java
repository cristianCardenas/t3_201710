package model.logic;

import model.data_structures.NoExisteElemento;
import model.data_structures.Stack;

public class logic {
	private Stack st;
	public logic()
	{
		st=new Stack<>();
	}
	public boolean expresionBienFormada (String expresion)
	{
		boolean bien=false;
		String e =expresion;
		if(e.startsWith("[")&&e.endsWith("]"))
		{
			char[]exp=e.toCharArray();
			for(int i =0;i<exp.length;i++)
			{
			
				int cont=0;
				if(exp[i]=="(".charAt(0))
				{
					cont++;
					for(int j=i+1;j<exp.length;j++)
					{
						int contado1=0;
						if (exp[j]==")".charAt(0))
						{
							contado1++;
							if(cont==contado1)
							{
								return bien=true;
							}
						}
					}
				}
			}
			
		}
		return bien;
	}

	public void  agregarStack(String c)
	{
		if(expresionBienFormada(c)==true)
		{
			
		
		for (int i=0;i<c.length();i++)
		{
			char k=c.charAt(i);
			st.push(k);
		}
		}
		
	
	}

	public Stack ordenarPila(Stack s) throws NoExisteElemento
	{
		 if (s.isEmpty()) {
		        return s;
		      }
		      int nodo = (int) s.pop();

		      // partition
		      Stack left  = new Stack<>();
		      Stack right = new Stack<>();
		      Stack mitad= new Stack<>();
		      Stack operadores= new Stack<>();
		      while(!s.isEmpty()) {
		        char y =  (char) s.pop();
		        if (y < nodo) {
		          left.push(y);
		        } 
		        if(y=="[".charAt(0)&&y=="]".charAt(0)&&y=="(".charAt(0)&&y==")".charAt(0))
		        {
		        	mitad.push(y);
		        }
		       if(y>nodo) {
		          right.push(y);
		        }
		       else
		       {
		    	   operadores.push(y);
		       }
		      }
		      ordenarPila(left);
		      ordenarPila(right);

		   
		      Stack tmp = new Stack();
		      while(!right.isEmpty()) {
		        tmp.push(right.pop());
		      }
		      tmp.push(nodo);
		      while(!left.isEmpty()) {
		        tmp.push(left.pop());
		      }
		      while(!tmp.isEmpty()) {
		        s.push(tmp.pop());
		      }
		      return s;
		    }
	
		
	
	}


